import React, { useState, useEffect } from "react";
import axiox from "./axios";
import movieTrailer from "movie-trailer";
import { IMAGE_BASE_URL } from "./constants";
import YouTube from "react-youtube";
import "./Row.css";
const _ = require('lodash');

function Row({ title, fetchUrl, isLargeRow }) {
  const [movies, setMovies] = useState([]);
  const [trailerUrl, setTrailerUrl] = useState("");
  useEffect(() => {
    async function getMovies() {
      const result = await axiox.get(fetchUrl);
      setMovies(result.data.results);
      return result;
    }
    getMovies();
  }, [fetchUrl]);

  const opts = {
    height: "390",
    width: "100%",
    playerVars: {
      autoplay: 1
    }
  };
  const handleImageClick = (movie) => {
    if (trailerUrl) {
      setTrailerUrl("");
    } else {
      movieTrailer(_.get(movie, "name", ""))
        .then((url) => {
          const urlParams = new URLSearchParams(new URL(url).search)
          setTrailerUrl(urlParams.get('v'))
        })
        .catch((e) => {
          console.log(e);
        });
    }
  };
  return (
    <div className="row">
      <h1>{title}</h1>
      <div className="row__posters">
        {movies.map((movie) => (
          <img
            onClick={() => handleImageClick(movie)}
            src={`${IMAGE_BASE_URL}${
              isLargeRow ? movie.poster_path : movie.backdrop_path
            }`}
            alt={movie.name}
            className={`row__poster ${isLargeRow && "row__posterLarge"}`}
            key={movie.id}
          />
        ))}
      </div>
      {trailerUrl && <YouTube videoId={trailerUrl} opts={opts} />}
      
    </div>
  );
}

export default Row;
