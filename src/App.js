import "./App.css";
import Row from "./Row";
import { MOVIE_TYPES } from "./constants";
import Banner from "./Banner";
import Nav from "./Nav"
const _ = require("lodash");
function App() {
  return (
    <div className="app">
      <Nav></Nav>
      <Banner/>
      {MOVIE_TYPES.map((movieType) => (
        <Row
          title={movieType.title}
          fetchUrl={movieType.fetchUrl}
          isLargeRow={_.get(movieType, "isLargeRow", false)}
        />
      ))}
    </div>
  );
}

export default App;
