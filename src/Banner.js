import React, { useState, useEffect } from "react";
import axios from "./axios";
import { requests, IMAGE_BASE_URL } from "./constants";
import "./Banner.css";
import { truncateString } from "./utils";
const _ = require("lodash");

function Banner() {
  const [movie, setMovie] = useState([]);

  useEffect(() => {
    async function fetchMovies() {
      const result = await axios.get(requests.fetchNetflixOriginals);

      let totalMoviesCount = result.data.results.length;
      let RandomMovie = result.data.results[_.random(totalMoviesCount - 1)];
      setMovie(RandomMovie);
    }
    fetchMovies();
  }, []);
  console.log(movie);
  return (
    <header
      className="banner"
      style={{
        backgroundSize: "cover",
        backgroundImage: `url(${IMAGE_BASE_URL}${_.get(
          movie,
          "backdrop_path"
        )})`,
        backgroundPosition: "center center",
      }}
    >
      <div className="banner__contents">
        <h1 className="banner__title">
          {_.get(movie, "title", "") ||
            _.get(movie, "name", "") ||
            _.get(movie, "original_name", "")}
        </h1>
        <div className="banner__buttons">
          <button className="banner__button">Play</button>
          <button className="banner__button">My list</button>
        </div>
        <h1 className="banner__description">
          {truncateString(_.get(movie, "overview", ""), 150)}
        </h1>
      </div>
      <div className="banner--fadeBottom"> </div>
    </header>
  );
}

export default Banner;
